/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author johnn
 */
@Entity
@Table(name = "detalhe_comanda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalheComanda.findAll", query = "SELECT d FROM DetalheComanda d")
    , @NamedQuery(name = "DetalheComanda.findByIdDetalheComanda", query = "SELECT d FROM DetalheComanda d WHERE d.idDetalheComanda = :idDetalheComanda")})
public class DetalheComanda implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDetalhe_Comanda")
    private Integer idDetalheComanda;
    @JoinColumn(name = "PacienteId", referencedColumnName = "idPaciente")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Paciente pacienteId;
    @JoinColumn(name = "EventoId", referencedColumnName = "idEvento")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evento eventoId;
    @JoinColumn(name = "ComandaId", referencedColumnName = "idComanda")
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Comanda comandaId;
    @Transient
    private Integer total;
    
    public DetalheComanda() {
    }

    public DetalheComanda(Paciente pac, Integer total){
        this.pacienteId = pac;
        this.total = total;
    }
    
    public DetalheComanda(Integer idDetalheComanda) {
        this.idDetalheComanda = idDetalheComanda;
    }

    public Integer getIdDetalheComanda() {
        return idDetalheComanda;
    }

    public void setIdDetalheComanda(Integer idDetalheComanda) {
        Integer oldIdDetalheComanda = this.idDetalheComanda;
        this.idDetalheComanda = idDetalheComanda;
        changeSupport.firePropertyChange("idDetalheComanda", oldIdDetalheComanda, idDetalheComanda);
    }

    public Paciente getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId(Paciente pacienteId) {
        Paciente oldPacienteId = this.pacienteId;
        this.pacienteId = pacienteId;
        changeSupport.firePropertyChange("pacienteId", oldPacienteId, pacienteId);
    }

    public Evento getEventoId() {
        return eventoId;
    }

    public void setEventoId(Evento eventoId) {
        Evento oldEventoId = this.eventoId;
        this.eventoId = eventoId;
        changeSupport.firePropertyChange("eventoId", oldEventoId, eventoId);
    }

    public Comanda getComandaId() {
        return comandaId;
    }

    public void setComandaId(Comanda comandaId) {
        this.comandaId = comandaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalheComanda != null ? idDetalheComanda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalheComanda)) {
            return false;
        }
        DetalheComanda other = (DetalheComanda) object;
        if ((this.idDetalheComanda == null && other.idDetalheComanda != null) || (this.idDetalheComanda != null && !this.idDetalheComanda.equals(other.idDetalheComanda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  pacienteId + " " + eventoId;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
