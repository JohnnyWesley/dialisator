/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author johnn
 */
@Entity
@Table(name = "comanda")
@XmlRootElement
public class Comanda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idComanda")
    private Integer idComanda;
    @Column(name = "dataComanda")
    @Temporal(TemporalType.DATE)
    private Date dataComanda;
    @Column(name = "total")
    private Integer total;
    @Column(name = "qntLinhaVenosa")
    private Integer qntLinhaVenosa;
    @Column(name = "qntLinhaArterial")
    private Integer qntLinhaArterial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comandaId", fetch = FetchType.LAZY)
    private List<DetalheComanda> detalheComandaList;

    public Comanda() {
    }

    public Comanda(Integer idComanda) {
        this.idComanda = idComanda;
    }

    public Integer getIdComanda() {
        return idComanda;
    }

    public void setIdComanda(Integer idComanda) {
        this.idComanda = idComanda;
    }

    public Date getDataComanda() {
        return dataComanda;
    }

    public void setDataComanda(Date dataComanda) {
        this.dataComanda = dataComanda;
    }
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getQntLinhaVenosa() {
        return qntLinhaVenosa;
    }

    public void setQntLinhaVenosa(Integer qntLinhaVenosa) {
        this.qntLinhaVenosa = qntLinhaVenosa;
    }

    public Integer getQntLinhaArterial() {
        return qntLinhaArterial;
    }

    public void setQntLinhaArterial(Integer qntLinhaArterial) {
        this.qntLinhaArterial = qntLinhaArterial;
    }
    
    

    @XmlTransient
    public List<DetalheComanda> getDetalheComandaList() {
        return detalheComandaList;
    }

    public void setDetalheComandaList(List<DetalheComanda> detalheComandaList) {
        this.detalheComandaList = detalheComandaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComanda != null ? idComanda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comanda)) {
            return false;
        }
        Comanda other = (Comanda) object;
        if ((this.idComanda == null && other.idComanda != null) || (this.idComanda != null && !this.idComanda.equals(other.idComanda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Comanda[ idComanda=" + dataComanda + " ]";
    }
    
}
