/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.DetalheComanda;
import model.Evento;
import utils.Singleton;
import model.Paciente;
/**
 *
 * @author johnn
 */
public class PacienteDAO {
    private EntityManager em;
    
    public PacienteDAO(){
    em = Singleton.getConnection();
}
    public void inserir (Paciente pac){
        em.getTransaction().begin();
        em.persist(pac);
        em.getTransaction().commit();
    }
    public void excluir(Paciente pac){
        em.getTransaction().begin();
        em.remove(pac);
        em.getTransaction().commit();
    }
    
    public void alterar(Paciente pac){
        em.getTransaction().begin();
        em.merge(pac);
        em.getTransaction().commit();
    }
    
    public List getListPac(){
       em.getTransaction().begin();
       Query query = em.createQuery("SELECT p FROM Paciente p where p.ativo = 1 order by p.nome");
       List<Paciente> lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
    
    public List getListPacArquivo(){
        em.getTransaction().begin();
       Query query = em.createQuery("SELECT p FROM Paciente p where p.ativo = 0");
       List<Paciente> lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
    
    public List getOnlyEvtPac(Evento evt, int mes, int year){;
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT p FROM Paciente p JOIN p.detalheComandaList d where d.eventoId =:evt and month(d.comandaId.dataComanda) =:mes and year(d.comandaId.dataComanda) =:year");
        query.setParameter("evt", evt);
        query.setParameter("mes", mes);
        query.setParameter("year", year);
        List<Paciente> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    public List getOnlyEvtPacWithoutMes(Evento evt){;
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT p FROM Paciente p JOIN p.detalheComandaList d where d.eventoId =:evt");
        query.setParameter("evt", evt);
        List<Paciente> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    
    public List getAllEvtPac(int mes, int year){;
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT p FROM Paciente p JOIN p.detalheComandaList d where month(d.comandaId.dataComanda) =:mes and year(d.comandaId.dataComanda) =:year group by p.idPaciente");
        query.setParameter("mes", mes);
        query.setParameter("year", year);
        List<Paciente> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
     public List getAllEvtPacWithoutMes(){;
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT p FROM Paciente p JOIN p.detalheComandaList d group by p.idPaciente");
        List<Paciente> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    public List getComandaListPac(Paciente pac, Evento evt, int mes, int year){
        em.getTransaction().begin();
        Query query =  em.createQuery("SELECT d from DetalheComanda d RIGHT JOIN d.pacienteId p where d.pacienteId =:pac and d.eventoId =:evt and month(d.comandaId.dataComanda) =:mes and year(d.comandaId.dataComanda) =:year");
        query.setParameter("pac", pac);
        query.setParameter("evt", evt);
        query.setParameter("mes", mes);
        query.setParameter("year", year);
        List<DetalheComanda> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    public List getComandaListPacAll(Paciente pac, int mes, int year){
        em.getTransaction().begin();
        Query query =  em.createQuery("SELECT d from DetalheComanda d RIGHT JOIN d.pacienteId p where d.pacienteId =:pac and month(d.comandaId.dataComanda) =:mes and year(d.comandaId.dataComanda) =:year");
        query.setParameter("pac", pac);
        query.setParameter("mes", mes);
        query.setParameter("year", year);
        List<DetalheComanda> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    public List getComandaListPacAllWithoutMes(Paciente pac){
        em.getTransaction().begin();
        Query query =  em.createQuery("SELECT d from DetalheComanda d RIGHT JOIN d.pacienteId p where d.pacienteId =:pac");
        query.setParameter("pac", pac);
        List<DetalheComanda> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
}


