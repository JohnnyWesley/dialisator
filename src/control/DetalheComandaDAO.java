/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Comanda;
import model.DetalheComanda;
import model.Evento;
import model.Paciente;
import utils.Singleton;

/**
 *
 * @author johnn
 */
public class DetalheComandaDAO {
    private EntityManager em;
    
    public DetalheComandaDAO(){
        em = Singleton.getConnection();
    }
    
    public void inserir(DetalheComanda dc){
        em.getTransaction().begin();
        em.persist(dc);
        em.getTransaction().commit();
    }
    
    public void excluir(DetalheComanda dc){
        em.getTransaction().begin();
        em.remove(dc);
        em.getTransaction().commit();
    }
    
    public List getList(Comanda c){
       em.getTransaction().begin();
       Query query = em.createQuery("SELECT d FROM DetalheComanda d where d.comandaId =:comanda");
       query.setParameter("comanda", c);
       List<DetalheComanda> lista = new ArrayList<>();
       lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
    
    public List detalheEvt(String pac, Evento evt, int mes, int ano){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT d FROM DetalheComanda d where d.pacienteId.nome =:pac and d.eventoId =:evt and month(d.comandaId.dataComanda) =:mes and year(d.comandaId.dataComanda) =:year");
        query.setParameter("pac", pac);
        query.setParameter("evt", evt);
        query.setParameter("mes", mes);
        query.setParameter("year", ano);
       List<DetalheComanda> lista = new ArrayList<>();
       lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
    
     public List detalheEvtWithoutMes(String pac, Evento evt){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT d FROM DetalheComanda d where d.pacienteId.nome =:pac and d.eventoId =:evt");
        query.setParameter("pac", pac);
        query.setParameter("evt", evt);
       List<DetalheComanda> lista = new ArrayList<>();
       lista = query.getResultList();
       em.getTransaction().commit();
       return lista;
    }
    
    
    public List detalheEvtAll(String pac, int mes, int ano){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT d from DetalheComanda d where d.pacienteId.nome =:pac and month(d.comandaId.dataComanda) = :mes and year(d.comandaId.dataComanda) =:year");
        query.setParameter("pac", pac);
        query.setParameter("mes", mes);
        query.setParameter("year", ano);
        List<DetalheComanda> lista = new ArrayList<>();
        lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
    public List detalheEvtAllBetweenDate(Date data1, Date data2){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT d from DetalheComanda d where d.comandaId.dataComanda BETWEEN :data1 and :data2 ORDER BY d.eventoId, d.pacienteId");
        query.setParameter("data1", data1);
        query.setParameter("data2", data2);
        List<DetalheComanda> lista = new ArrayList<>();
        lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
     public List detalheEvtAllWithoutMes(String pac){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT d from DetalheComanda d where d.pacienteId.nome =:pac");
        query.setParameter("pac", pac);
        List<DetalheComanda> lista = new ArrayList<>();
        lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
    
}
