/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Comanda;
import utils.Singleton;

/**
 *
 * @author johnn
 */
public class ComandaDAO {
    private EntityManager em;
    
    public ComandaDAO(){
        em = Singleton.getConnection();
    }
    
    public int inserir(Comanda c){
        em.getTransaction().begin();
        em.persist(c);
        em.flush();
        em.getTransaction().commit();
        return c.getIdComanda();
    }
    
    public void alterar(Comanda c){
        em.getTransaction().begin();
        em.merge(c);
        em.getTransaction().commit();
    }
    
    public List getComandaList(Date date){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT c FROM Comanda c where c.dataComanda =:date");
        query.setParameter("date", date);
        List<Comanda> list = query.getResultList();
        em.getTransaction().commit();
        return list; 
    }
    
    public List getComandaList1(){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT c FROM Comanda c order by c.dataComanda");
        List<Comanda> list = query.getResultList();
        em.getTransaction().commit();
        return list; 
    }
    
}
