/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Evento;
import utils.Singleton;

/**
 *
 * @author johnn
 */
public class EventoDAO {
    private EntityManager em;
    public EventoDAO(){
        em = Singleton.getConnection();
    }
    
    public List getEventList(){
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT e FROM Evento e");
        List<Evento> lista = query.getResultList();
        em.getTransaction().commit();
        return lista;
    }
}
