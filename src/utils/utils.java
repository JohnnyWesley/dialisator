/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author johnn
 */
public class utils {
    
    public static String convertData(Date date){
      return new  SimpleDateFormat("dd/MM/yyy").format(date);
    }
    
    public static String convertDataSql(Date date){
      return new  SimpleDateFormat("yyyy-MM-dd").format(date);
    }
    
}
